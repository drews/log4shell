# log4shell

## Overview

This page is intended to share and collect information on helping the Duke Linux
community stay safe from
[CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)

Vulnerable versions of log4j include versions between 2.0 and 2.15 (2.15
contains the fix)

## Mitigations

If your software cannot be updated, you can mitigate by doing one of the
following:

* Run your service with `-Dlog4j2.formatMsgNoLookups=true`, or set the environment variable: `LOG4J_FORMAT_MSG_NO_LOOKUPS=true`. This is only applicable for log4j 2.10 and above

* Remove the Jndi class from the jar file with: `zip -q -d log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class.`. This will work on all versions of log4j

Note that both of the above require a service restart.

## Tools

There are a number of tools popping up now to help find these issues. We have found the following useful:

[grype](https://github.com/anchore/grype) - Runs on all oses, you can do a `grype dir:/your/java/dir` to find vulnerable jars

[ansible-playbook](./log4j-ps.yaml) - Grabs log4j out of process listing, ignoring processes that have the mitigation in the process list